This just adds tokens for events

event-start  -  The formatted version of the start date/time
event-start-time  -  The formatted start time
event-start-date  -  The formatted start date
event-start-yyyy  -  Event Start year (four digit)
event-start-yy  -  Event Start year (two digit)
event-start-month  -  Event Start month (full word)
event-start-mon  -  Event Start month (abbreviated)
event-start-mm  -  Event Start month (two digit, zero padded)
event-start-m  -  Event Start month (one or two digit)
event-start-ww  -  Event Start week (two digit)
event-start-day  -  Event Start day (full word)
event-start-ddd  -  Event Start day (abbreviation)
event-start-dd  -  Event Start day (two digit, zero-padded)
event-start-d  -  Event Start day (one or two digit)

event-end  -  The formatted version of the end date/time
event-end-time  -  The formatted end time
event-end-date  -  The formatted end date
event-end-yyyy  -  Event End year (four digit)
event-end-yy  -  Event End year (two digit)
event-end-month  -  Event End month (full word)
event-end-mon  -  Event End month (abbreviated)
event-end-mm  -  Event End month (two digit, zero padded)
event-end-m  -  Event End month (one or two digit)
event-end-ww  -  Event End week (two digit)
event-end-day  -  Event End day (full word)
event-end-ddd  -  Event End day (abbreviation)
event-end-dd  -  Event End day (two digit, zero-padded)
event-end-d  -  Event End day (one or two digit)